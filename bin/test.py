# -- coding: utf-8 --
# @Time : 2021/9/15  9:23 上午
# @Author : wangdexin
# @File : test.py
# import re
#
# a = ['日本进口机芯正品NIKKO尼康机械节拍器钢琴吉他节奏器通用']
#
# for value in a:
#     print(value)
#     b = re.search('NIKKO', value)
#     print(b)

# import time
# from apscheduler.schedulers.blocking import BlockingScheduler
# def job():
#     print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))
# if __name__ == '__main__':
#     # 该示例代码生成了一个BlockingScheduler调度器，使用了默认的任务存储MemoryJobStore，以及默认的执行器ThreadPoolExecutor，并且最大线程数为10。
#     # BlockingScheduler：在进程中运行单个任务，调度器是唯一运行的东西
#     scheduler = BlockingScheduler()
#     # 采用阻塞的方式
#     # 采用固定时间间隔（interval）的方式，每隔5秒钟执行一次
#     scheduler.add_job(job, 'interval', seconds=5)
#     scheduler.start()

# import time
# from apscheduler.schedulers.background import BackgroundScheduler
# def job():
#     print('job:', time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))
# if __name__ == '__main__':
#     # BackgroundScheduler: 适合于要求任何在程序后台运行的情况，当希望调度器在应用后台执行时使用。
#     scheduler = BackgroundScheduler()
#     # 采用非阻塞的方式
#     # 采用固定时间间隔（interval）的方式，每隔3秒钟执行一次
#     scheduler.add_job(job, 'interval', seconds=3)
#     # 这是一个独立的线程
#     scheduler.start()
#     # 其他任务是独立的线程
#     while True:
#         print('main-start:', time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))
#         time.sleep(2)
#         print('main-end:', time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))

# 13.127.29.206 外网ip
# 172.31.28.31 内网ip
import requests,json
data = {"md5":"qunaer_df7d998b0688c85b2d793b86aac5a770"}
res = requests.post("http://13.127.29.206:1106/get/rank",data = json.dumps(data))
print(res.text)








