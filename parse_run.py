# -- coding: utf-8 --
# @Time : 2021/9/18  4:42 下午
# @Author : wangdexin
# @File : parse_run.py
from sanic import Sanic,response
from sanic import Blueprint
from apscheduler.schedulers.background import BackgroundScheduler
import time
from biz.biz_detail import Detail
import random
sanic_app = Sanic(name="qury")

bp_search = Blueprint(name='db', url_prefix='/parse', strict_slashes=True)

# from sanic_openapi import swagger_blueprint
# sanic_app.blueprint(swagger_blueprint)


@bp_search.route('/v1',methods=["GET"])
def find(request):
    return response.json({"code":"00101"})


# async def do():
#     print("do")
#
# def job():
#     print('job:', time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))
# def job2():
#     print('job2:', time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())))


# @sanic_app.route("/",methods=["GET"])
# async def test(request):
#     scheduler = BackgroundScheduler()
#     scheduler.add_job(job, 'interval', seconds=3)
#     print("1",scheduler.get_jobs())
#     scheduler.start()
#     return response.json({"code":"00101"})


# @sanic_app.route("/task2",methods=["GET"])
# async def test(request):
#
#     scheduler = BackgroundScheduler()
#     print("2",scheduler.get_jobs())
#     scheduler.add_job(job2, 'interval', seconds=3)
#     print("2",scheduler.get_jobs())
#
#     scheduler.start()
#     return response.json({"code":"00101"})



# @sanic_app.route("/html",methods=["GET"])
# async def html(request):
#     return response.html('Welcom to 猿人学Python',headers={'X-Serverd-By': 'YuanRenXue Python'})

sanic_app.static("/main",'index.html', content_type="text/html; charset=utf-8")
sanic_app.static("/parse",'index2.html', content_type="text/html; charset=utf-8")




# @sanic_app.route("/get/rank",methods=["POST"])
# async def get_rank(request):
#     items = request.json
#     id = items.get("md5","")
#     try:
#         result = Detail().find_one(id)
#         rank = result.get("rank","")
#         page = result.get("page","")
#         if rank=="":
#             rank  = page
#     except:
#         rank = "30"
#     return response.json({"code":"200","rank":rank})


if __name__ == '__main__':
    sanic_app.run(host='127.0.0.1', port=1106)


