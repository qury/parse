# -- coding: utf-8 --
# @Time : 2021/9/14  12:03 下午
# @Author : wangdexin
# @File : vip_items_goods_details.py




template = {
    "version" : "202100905-vip-shop",
    "fields_rules" : {
        "title" : {
            "field" : "title",
            "depict" : "标题",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..goodsName",
            "value_filter" : []
        },
        "img" : {
            "field" : "img",
            "depict" : "图片",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..s3_img",
            "value_handle" : []
        },
        "fa_name" : {
            "field" : "fa_name",
            "depict" : "fa名称",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..fa_name",
            "value_handle" : []
        },
        "s_path" : {
            "field" : "s_path",
            "depict" : "采集路径",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..s_path",
            "value_handle" : []
        },
        "rank" : {
            "field" : "rank",
            "depict" : "页面排序",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..rank",
            "value_handle" : []
        },
        "source" : {
            "field" : "source",
            "depict" : "来源类别",
            "type" : "",
            "select" : "static",
            "regex" : "path",
            "value_handle" : [
            ]
        },
        "md5" : {
            "field" : "md5",
            "depict" : "items爬虫唯一ID",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..id",
            "value_handle" : []
        },
        "url" : {
            "field" : "url",
            "depict" : "items的H5地址",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..url",
            "value_handle" : []
        },
        "tags" : {
            "field" : "tags",
            "depict" : "商品品牌",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..brandStoreName",
            "value_handle" : []
        },
        "date_str" : {
            "field" : "date_str",
            "depict" : "发布时间",
            "type" : "",
            "select" : "static",
            "regex" : "",
            "value_handle" : [
            ]
        },
        "vers" : {
            "field" : "vers",
            "depict" : "版本号",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..version",
            "value_handle" : []
        },
        "area" : {
            "field" : "area",
            "depict" : "地点",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..areaOutput",
            "value_handle" : []
        },
        "discount" : {
            "field" : "discount",
            "depict" : "折扣",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..agio",
            "value_handle" : []
        },
        "product_parm" : {
            "field" : "product_parm",
            "depict" : "产品参数",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..props",
            "value_handle" : []
        },
        "t_tags" : {
            "field" : "t_tags",
            "depict" : "商品标签",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..s_path",
            "value_handle" : [
                {
                    "name" : "path_tag_all",
                    "value" : "",
                    "regex" : ""
                }
            ]
        },
        "price" : {
            "field" : "price",
            "depict" : "价格",
            "type" : "",
            "select" : "jpath",
            "regex" : "$..vipshopPrice",
            "value_handle" : []
        }
    }
}