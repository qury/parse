# -- coding: utf-8 --
# @Time : 2021/9/17  4:31 下午
# @Author : wangdexin
# @File : fields.py
'''
字段校验，
'''
import six
import datetime
import time

from marshmallow import (
    Schema,
    fields,
    ValidationError,
    validate
)


def check_publish_time_str(v):
    if v:
        try:
            datetime.datetime.strptime(v, "%Y-%m-%d %H:%M:%S")
        except ValidationError:
            raise ValueError("Incorrect data format, "
                             "{data} should be YYYY-MM-DD HH:MM:SS".format(data=v))

def check_publish_time_stamp(v):
    """
    校验发布时间
    :param v:
    :return:
    """
    if v:
        try:
            if len(str(v)) != 13:
                raise ValueError("Incorrect data format, "
                                 "{data} should be 13 numbers".format(data=v))
        except:
            pass

def check_is_str(v):
    """
    验证是否是字符串
    :param v:
    :return:
    """
    if not v or not isinstance(v, (six.text_type, six.string_types)):
        raise ValueError("Incorrect data format, "
                                 "{data} should be string".format(data=v))

class ImgSchema(Schema):
    '''
    检测图片链接是否合法
    '''
    img = fields.Url(required=True)




class baseSchema(Schema):
    '''
    字段格式，值校验
    '''
    fa_name  = fields.Str(required=True)
    s_path    = fields.Str(required=True)
    source    = fields.Str(required=False, missing="")
    md5     = fields.Str(required=True)
    # url = fields.Str(required=True,)
    url = fields.Url(required=True)
    vers = fields.Str(required=True)
    title = fields.Str(required=True,validate=validate.Length(min=1))
    # img = fields.Str(required=False,missing='')
    rank = fields.Str(required=True)
    date_str = fields.Str(required=False,missing='')

if __name__ == '__main__':
    data = {'title': "我", 'img': 'https://www.baidu.com/', 'fa_name': '安居客', 's_path': '安居客*二手房*北京*朝阳*百子湾', 'rank': '56', 'source': 'path',
            'md5': 'anjuke_062da0c434c40616b6d605ae734543bc', 'url': 'https://m.anjuke.com/bj/sale/A5960731005/',
            'tags': '满五 唯一住房 集体供暖', 'date_str': '2021-09-17 00:00:00', 'vers': '20210822-anjuke-esf', 'area': '朝阳',
            'bus_district': '百子湾', 'city': '北京', 'unit_type': '2室2厅2卫', 'price_str': '700 万', 'acreage': '98㎡',
            'addr': '',
            'company': '麦田房产', 'subway': '', 'floor': '中层(共7层)', 'community': '沿海赛洛城(南区)', 't_type': ''}

    r = ImgSchema().load({"img":"https://www.baidu.com/"})
    print(r)




