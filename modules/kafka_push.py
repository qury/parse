# -- coding: utf-8 --
# @Time : 2021/9/18  9:58 上午
# @Author : wangdexin
# @File : kafka_push.py
from kafka import KafkaProducer
import json,traceback

class Kafka_push(object):
    def __init__(self):
        self.producer_data = KafkaProducer(
            bootstrap_servers=['h-kafka.inner.qury.me:9092'],
            api_version=(0, 10),
            value_serializer=lambda v: json.dumps(v).encode('utf-8'),
            key_serializer=lambda v: v.encode('utf-8'),
            security_protocol='SASL_PLAINTEXT', sasl_mechanism='SCRAM-SHA-256',
            sasl_plain_username="producer", sasl_plain_password='Iex@Ac@m&9U**Q%9')

    def send_data2kafka(self, value,fa_name):
        """
        示例： kafka_producer.send('world', {'key1': 'value1'})
        :param topic:     kafka 队列名称
        :param value:     python 字典类型
        :return:
        """
        topic = "h_item"
        print("fa_name",fa_name)
        try:
            a = self.producer_data.send(topic,key=fa_name,value=value)
            a.get(timeout=30)
            print(a)
            # self.producer_data.close()
        except Exception as e:
            if isinstance(e, BufferError):
                print(e)
            else:
                traceback.print_exc()

    def delivery_report(self,err, msg):
        if err is not None:
            print('Message delivery failed: {}'.format(err))
        else:
            print(str(msg.value(), "utf-8"))





