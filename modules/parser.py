# -- coding: utf-8 --
# @Time : 2021/9/14  12:08 下午
# @Author : wangdexin
# @File : parser.py

import re,json
import chardet,jsonpath
from scrapy.selector import Selector
from lxml import etree
'''解析器'''
class Parser():
    def __init__(self, *args, **kwargs):
        self.response = kwargs.get("response")
        self.type = kwargs.get("select")
        self.regex = kwargs.get("regex")
        # self.data_dic = kwargs.get("data")
    def parser(self):
        return getattr(self, self.type)()
    def xpath(self, *args, **kwargs):
        '''源码类型文本，xpath解析'''
        parse_data = [""]
        if self.regex:
            regexs = self.regex.split("&")
            try:
                parse_data = Selector(self.response).xpath(regexs[0]).extract()
                if not parse_data and len(regexs) > 1:
                    parse_data = self.response.xpath(regexs[1]).extract()
            except Exception as e:
                print('parser',e)
        return parse_data
    def rpath(self, *args, **kwargs):
        '''源码类型文本，正则解析'''
        try:
            body = self.response.text
            detector = chardet.detect(str(body))
            if detector.get("encoding") == "ascii":
                body = str(body).decode("unicode-escape")
        except Exception as e:
            body = str(self.response.text)
        parse_data = re.findall(self.regex, body)
        return parse_data
    def jpath(self, *args, **kwargs):
        '''字典类型文本，json解析'''
        if isinstance(self.response,str):
            body = json.loads(self.response)
        elif isinstance(self.response,dict):
            body = self.response
        else:
            body = {}
        try:
            result = jsonpath.jsonpath(body,self.regex)
            if len(result)>1:
                result = result[0:1]
        except Exception as e:
            result = []
            print('parser——2',e)
        if not result :
            result = []
        return result


    def static(self, *args, **kwargs):
        """
        默认值
        """
        value = self.regex
        return value



