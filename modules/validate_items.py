# -- coding: utf-8 --
# @Time : 2021/9/17  5:58 下午
# @Author : wangdexin
# @File : validate_items.py

from modules.fields import (
    ImgSchema,
    baseSchema,
    ValidationError
)


class BaseItem(object):
    def __init__(self):
        pass

    def items_check(self,data):
        items = data
        img = data.get("img","")
        chose_items = self.items_delete(items)
        Img_items = {"img":img}
        try:
            base_resutl = baseSchema().load(chose_items)
        except ValidationError as e:
            base_resutl = False
            print(e.messages)
        try:
            img_resutl = ImgSchema().load(Img_items)
        except ValidationError as e:
            img_resutl = False
            print(e.messages)

        if base_resutl and img_resutl:
            return data
        else:
            print("数据格式错误")




    def items_delete(self,items):
        '''
        删除非验证的key
        :param items:
        :return:
        '''
        check_keys_list = ["fa_name", "s_path", "source", "md5", "url", "vers", "title", "rank", "date_str"]
        for i in list(items.keys()):
            if i not in check_keys_list:
                items.pop(i)
        return items

if __name__ == '__main__':
    basei = BaseItem()
    data = {'title': "我", 'img': 'htts://www.baidu.com/', 'fa_name': '安居客', 's_path': '安居客*二手房*北京*朝阳*百子湾', 'rank': '56', 'source': 'path',
            'md5': 'anjuke_062da0c434c40616b6d605ae734543bc', 'url': 'https://m.anjuke.com/bj/sale/A5960731005/',
            'tags': '满五 唯一住房 集体供暖', 'date_str': '2021-09-17 00:00:00', 'vers': '20210822-anjuke-esf', 'area': '朝阳',
            'bus_district': '百子湾', 'city': '北京', 'unit_type': '2室2厅2卫', 'price_str': '700 万', 'acreage': '98㎡',
            'addr': '',
            'company': '麦田房产', 'subway': '', 'floor': '中层(共7层)', 'community': '沿海赛洛城(南区)', 't_type': ''}

    basei.items_check(data)





