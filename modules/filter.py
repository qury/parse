# -- coding: utf-8 --
# @Time : 2021/9/14  12:08 下午
# @Author : wangdexin
# @File : filter.py
import re,sys
import json,datetime,time
'''过滤器,包含特殊词，特殊字符的内容过滤掉，(全部丢掉，置空)'''
class Filter(object):
    def __init__(self, *args, **kwargs):
        self.parm = kwargs.get("parm", [])
        self.data = kwargs.get("data", [])

    def filter(self):
        value_filter = self.parm
        data = self.data
        for i in range(len(value_filter)):
            name = value_filter[i].get("name")
            value = value_filter[i].get("value")
            regex = value_filter[i].get("regex")
            if i == 0:
                data = getattr(self, name)(data=self.data, value=value, regex=regex)
            else:
                data = getattr(self, name)(data=data, value=value, regex=regex)
        return data
    """
    正则过滤器
    """
    def regular(self, *args, **kwarg):
        return [value for value in kwarg.get("data", []) if not re.search(kwarg.get("regex"), value)]
    """
    空过滤器( "", '', None, 0, False)
    """
    def none(self, *args, **kwarg):
        return [value for value in kwarg.get("data", []) if value]
