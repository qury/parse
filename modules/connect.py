# -- coding: utf-8 --
# @Time : 2021/9/26  6:44 下午
# @Author : wangdexin
# @File : connect.py
from motor.motor_asyncio import AsyncIOMotorClient
from functools import wraps
import urllib.parse
from pymongo import MongoClient




def singleten(cls):
    _instances = {}
    @wraps(cls)
    def instance(*args, **kw):
        if cls.__name__ not in _instances:
            _instances[cls.__name__] = cls(*args, **kw)
        return _instances[cls.__name__]
    return instance

#
#
# @singleten
# class Mongo(object):
#     __motor = None
#
#     def __init__(self, uri='', *, loop=None):
#         self.uri = uri
#         self.loop = loop
#
#     def get_connecttion(self):
#         if self.__motor is None:
#             self.__motor = AsyncIOMotorClient(self.uri, io_loop=self.loop, serverSelectionTimeoutMS=100)
#         return self.__motor
#
#     def get_db(self):
#         if self.__motor is None:
#             self.get_connecttion()
#         return self.__motor[self.__motor.delegate._MongoClient__default_database_name]
#
#     def get_collection(self, *, collection):
#         if self.__motor is None:
#             self.get_connecttion()
#         return self.get_db()[collection]

class QuryMongo(object):
    def __init__(self):
        self.client = MongoClient(
        'mongodb://QurySpiderMongo:GI5oT8yckcFSGiwW@172.31.25.104:27099')
        # self.client = MongoClient("mongodb://admin:" + urllib.parse.quote_plus(
        #     "Qury666!@#") + "@13.127.29.206:27088")
    def get_connecttion(self):
        db = self.client["h_spider"]
        collection = db['spider_data']
        return collection
if __name__ == '__main__':
    aa = QuryMongo()
    jjj =  aa.get_connecttion()
    print(jjj)









