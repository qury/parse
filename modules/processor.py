# -- coding: utf-8 --
# @Time : 2021/9/14  12:08 下午
# @Author : wangdexin
# @File : processor.py
import re, sys, datetime, time
from urllib.parse import urljoin

'''处理器，可根据任务添加处理器函数，'''
class Processor(object):
    def __init__(self,**kwargs):
        self.parm = kwargs.get("parm", [])
        self.data = kwargs.get("data")
    def processor(self):
        value_handle = self.parm
        data = self.data
        for i in range(len(value_handle)):
            name = value_handle[i].get("name", "")
            value = value_handle[i].get("value", "")
            regex = value_handle[i].get("regex", "")
            try:
                if i == 0:
                    data = getattr(self, name)(data=data, value=value, regex=regex)
                else:
                    data = getattr(self, name)(data=data, value=value, regex=regex)
            except Exception as e:
                print("processor",e)
        return data
    def prefix(self, *args, **kwargs):
        """
        前缀处理
        """
        return str(kwargs.get("value")) + str(kwargs.get("data"))
    def postfix(self, *args, **kwargs):
        """
        后缀处理
        """
        return str(kwargs.get("data")) + str(kwargs.get("value"))
    def replace(self, *args, **kwargs):
        """
        替换处理
        """
        data = re.sub(kwargs.get("regex"), str(kwargs.get("value")), str(kwargs.get("data")))
        if kwargs.get("data") == data:
            data = re.sub(kwargs.get("regex"), str(kwargs.get("value")), kwargs.get("data"))
        return data

    def extract(self, *args, **kwargs):
        """
        提取处理，可根据正则提取数据
        """
        pat = re.compile(kwargs.get("regex").encode("utf-8"))
        data_list = pat.findall(kwargs.get("data").encode("utf-8"))
        value = len(data_list) > 0 and data_list.pop() or ""
        return value

    def extract_all(self, *args, **kwargs):
        """
        提取所有符合规则的数据
        """
        data_list = re.findall(kwargs.get("regex"), str(kwargs.get("data")))
        value = len(data_list) > 0 and ' '.join(data_list) or ""
        return value


    def static(self, *args, **kwargs):
        """
        默认值
        """
        data = kwargs.get("data")
        print("static",kwargs.get("value"))
        return str(kwargs.get("value"))
    def strip(self, *args, **kwarg):
        """
        前后空格处理
        """
        return [value.strip() for value in kwarg.get("data", []) if value]

    def path_tag_all(self,*args, **kwargs):
        '''
        路径标签
        :return:
        '''
        data = kwargs.get("data")
        tags_list = data.split("*")
        return ' '.join(tags_list)

    def urljoin(self,*args,**kwargs):
        '''
        url类型数据处理，前缀补充，合法校验
        '''
        data = kwargs.get("data")
        domain = str(kwargs.get("value"))
        if isinstance(data, list):
            result = []
            for i in data:
                result.append(urljoin(domain, str(i)))
        else:
            result = urljoin(domain, str(kwargs.get("data")))
        return result
    def join(self,*args,**kwargs):
        '''
        列表类型，join处理
        '''
        regex = kwargs.get("regex", ",")
        data = kwargs.get("data")
        data = map(str, data)
        data = regex.join(data)
        return data




